package com.example.pahlawan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pahlawan.model.Pahlawan
import java.util.ArrayList

class RecycleView : AppCompatActivity(), OnHeroClickListener {
    var listHeroAdapter: ListHeroAdapter? = null
    val heroes = ArrayList<Pahlawan>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycle_view)
        val recycleView = findViewById<RecyclerView>(R.id.rcvImageHero)

        initiateData()
        val linearlayout = LinearLayoutManager(this)
        linearlayout.orientation = LinearLayoutManager.VERTICAL
        recycleView.layoutManager = linearlayout

        listHeroAdapter = ListHeroAdapter(heroes, this)
        recycleView.adapter = listHeroAdapter

    }

    private fun initiateData(){
        val imageString = resources.getStringArray(R.array.data_photo_string)
        val descriptionString = resources.getStringArray(R.array.data_description)

        for (i in imageString.indices){
            heroes.add(Pahlawan(imageString.get(i), descriptionString.get(i)))
        }
    }

    override fun onItemClick(item: Pahlawan, position: Int) {
        Toast.makeText(this, item.description, Toast.LENGTH_SHORT).show()
    }
}