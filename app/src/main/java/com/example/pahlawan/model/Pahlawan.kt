package com.example.pahlawan.model

data class Pahlawan (val image: String, val description: String)