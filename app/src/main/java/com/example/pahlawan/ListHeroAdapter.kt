package com.example.pahlawan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pahlawan.model.Pahlawan

class ListHeroAdapter(val listImages: ArrayList<Pahlawan>, var clickListener: OnHeroClickListener): RecyclerView.Adapter<ListHeroAdapter.ListHeroViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListHeroAdapter.ListHeroViewHolder {
        return ListHeroViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_hero, parent, false))
    }

    override fun getItemCount(): Int {
        return listImages.size
    }

    override fun onBindViewHolder(holder: ListHeroViewHolder, position: Int) {
        val listImage = listImages.get(position)

        listImage.let{
            Glide.with(holder.itemView).load(it.image).into(holder.imgHero)
            holder.textHero.text = it.description.substring(0,100)+"..."
        }

        holder.initialize(listImages.get(position), clickListener)
    }
    inner class ListHeroViewHolder(view: View): RecyclerView.ViewHolder(view){
        val imgHero = view.findViewById<ImageView>(R.id.imgItemHero)
        val textHero = view.findViewById<TextView>(R.id.textItemHero)

        fun initialize(item: Pahlawan, action: OnHeroClickListener){
            textHero.text = item.description

            itemView.setOnClickListener {
                action.onItemClick(item, absoluteAdapterPosition)
            }
        }
    }
}

interface OnHeroClickListener{
    fun onItemClick(item: Pahlawan, position: Int)
}